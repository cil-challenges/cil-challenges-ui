import React from "react"

const CommentList = ({ comment }) => (
    <div>
        <p>{comment.from} - </p>
        <p id="commentFormat">{comment.comment}</p>
    </div>
)

export default CommentList